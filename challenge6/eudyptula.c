#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/kdev_t.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

#define BUFFER_LEN 13

static struct miscdevice eudyptula_misc_device;
static char *id = "5d6dd44e7505";

static int __init eudyptula_misc_init(void)
{
	int ret;

	ret = misc_register(&eudyptula_misc_device);
	if (ret < 0)
		pr_err("Failed to register eudyptula misc device\n");

	return ret;
}

static void __exit eudyptula_misc_cleanup(void)
{
	pr_debug("Goodbye!\n");
	misc_deregister(&eudyptula_misc_device);
}

static ssize_t eudyptula_read(struct file *file, char __user *buff,
		size_t count, loff_t *ppos)
{
	return simple_read_from_buffer(buff, count, ppos, id, strlen(id));
}

static ssize_t eudyptula_write(struct file *file, const char __user *buff,
		size_t count, loff_t *pos)
{
	char received[BUFFER_LEN];
	ssize_t ret = 0;

	ret = simple_write_to_buffer(&received, BUFFER_LEN - 1, pos,
			buff, count);
	received[count] = '\0';

	if (ret != count) {
		pr_err("copy to user failed\n");
		return ret;
	}


	if (strncmp(received, id, BUFFER_LEN)) {
		pr_err("received incorrect identifier %s\n", received);
		return -EINVAL;
	}


	pr_debug("received correct identifier\n");

	return ret;
}


static const struct file_operations eudyptula_fops = {
	.owner = THIS_MODULE,
	.read  = eudyptula_read,
	.write = eudyptula_write,
};


static struct miscdevice eudyptula_misc_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name  = "eudyptula",
	.fops  = &eudyptula_fops,
};

module_init(eudyptula_misc_init);
module_exit(eudyptula_misc_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Frank Vanbever");
MODULE_DESCRIPTION("Eudyptula Misc Char Device");
