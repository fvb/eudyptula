#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define  BUFFER_LEN 13

static char id[BUFFER_LEN];

int open_file(void)
{
	int fd = open("/dev/eudyptula", O_RDWR);

	if (fd < 0) {
		perror("Failed to open device\n");
		return errno;
	}
	return fd;
}


int main(void)
{
	int ret, fd;
	char *wrong = "hello";

	fd = open_file();
	printf("Attempting read test\n");
	ret = read(fd, &id, BUFFER_LEN);
	close(fd);
	if (ret < 0) {
		perror("Failed to read from device");
		return errno;
	}
	printf("Received id is %s\n", id);

	fd = open_file();
	printf("Attempting correct write test\n");
	ret = write(fd, &id, strlen(id));
	close(fd);
	if (ret < 0) {
		perror("Failed to write to device\n");
		return errno;
	}

	printf("Write succeeded\n");


	printf("Attempting wrong write test\n");
	fd = open_file();
	ret = write(fd, wrong, strlen(wrong));
	close(fd);
	if (ret < 0) {
		perror("Failed to write to device\n");
		close(fd);
		return errno;
	}

	printf("Write succeeded\n");


	return 0;
}


