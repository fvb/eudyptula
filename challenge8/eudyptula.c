#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/debugfs.h>
#include <linux/jiffies.h>
#include <linux/gfp.h>
#include <linux/spinlock.h>

#define BUFFER_LEN 13
#define JIFFIES_LEN 256

struct dentry *eudyptula_dir;
struct dentry *id_file;
struct dentry *jiffies_file;
struct dentry *foo_file;

static char *id = "5d6dd44e7505";
static void *foo_page;
static rwlock_t foo_lock;



static ssize_t id_read(struct file *file, char __user *buff,
		size_t count, loff_t *ppos)
{
	return simple_read_from_buffer(buff, count, ppos, id, strlen(id));
}

static ssize_t id_write(struct file *file, const char __user *buff,
		size_t count, loff_t *pos)
{
	char received[BUFFER_LEN];
	ssize_t ret = 0;

	ret = simple_write_to_buffer(&received, BUFFER_LEN - 1, pos,
			buff, count);
	received[count] = '\0';

	if (ret != count) {
		pr_err("copy to user failed\n");
		return ret;
	}


	if (strncmp(received, id, BUFFER_LEN)) {
		pr_err("received incorrect identifier %s\n", received);
		return -EINVAL;
	}


	pr_debug("received correct identifier\n");

	return ret;
}

static ssize_t jiffies_read(struct file *file, char __user *buff,
		size_t count, loff_t *ppos)
{
	char jiffiestring[JIFFIES_LEN] = { 0 };

	snprintf(jiffiestring, JIFFIES_LEN, "%lu", jiffies);
	return simple_read_from_buffer(buff, count, ppos, jiffiestring,
			strlen(jiffiestring));
}

static ssize_t foo_read(struct file *file, char __user *buff,
		size_t count, loff_t *ppos)
{
  unsigned long flags;
  ssize_t ret = 0;

	read_lock_irqsave(&foo_lock, flags);
	ret = simple_read_from_buffer(buff, count, ppos, foo_page, PAGE_SIZE);
	read_unlock_irqrestore(&foo_lock, flags);

  return ret;
}


static ssize_t foo_write(struct file *file, const char __user *buff,
		size_t count, loff_t *pos)
{
	ssize_t ret = 0;
  unsigned long flags;

  pr_debug("Foo write");
	write_lock_irqsave(&foo_lock, flags);
	ret = simple_write_to_buffer(foo_page, PAGE_SIZE, pos,
			buff, count);
	write_unlock_irqrestore(&foo_lock, flags);


	if (ret != count) {
		pr_err("copy to user failed\n");
		return ret;
	}
	return ret;
}
static const struct file_operations id_fops = {
	.owner = THIS_MODULE,
	.read  = id_read,
	.write = id_write,
};

static const struct file_operations jiffies_fops = {
	.owner = THIS_MODULE,
	.read  = jiffies_read,
};

static const struct file_operations foo_fops = {
	.owner = THIS_MODULE,
	.read  = foo_read,
	.write = foo_write,
};


static int __init hello_init(void)
{
	printk(KERN_DEBUG "Hello World!\n");

	eudyptula_dir = debugfs_create_dir("eudyptula", NULL);

	if(!eudyptula_dir) {
		pr_err("Eudyptula: failed to create debugfs directory");
		return -1;
	}

	id_file = debugfs_create_file("id", 0666, eudyptula_dir, NULL,
			&id_fops);
	if(!id_file) {
		pr_err("Eudyptula: failed to create ID file");
		return -1;
	}

	jiffies_file = debugfs_create_file("jiffies", 0444, eudyptula_dir, NULL,
					   &jiffies_fops);
	if(!jiffies_file) {
		pr_err("Eudyptula: failed to create jiffies file");
		return -1;
	}

	pr_debug("Creating foo file");
	foo_file = debugfs_create_file("foo", 0664, eudyptula_dir, NULL,
			&foo_fops);
	if(!foo_file) {
		pr_err("Eudyptula: failed to create foo file");
		return -1;
	}

	pr_debug("Creating foo lock");
	rwlock_init(&foo_lock);

	pr_debug("allocating foo page");
	foo_page = (void *) get_zeroed_page(GFP_KERNEL);
	if(!foo_page) {
		pr_err("Eudytpula: failed to allocate foo page");
		return -1;
	}


	return 0;
}

static void __exit hello_cleanup(void)
{
	debugfs_remove_recursive(eudyptula_dir);
	free_page((unsigned long) foo_page);
	printk(KERN_DEBUG "Goodbye!\n");
}

module_init(hello_init);
module_exit(hello_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Frank Vanbever");
MODULE_DESCRIPTION("Eudyptula debugfs module");
