#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define  BUFFER_LEN 13
#define  JIFFIES_LEN 256

static char id[BUFFER_LEN];

int open_file(char *path)
{
  int fd = open(path, O_RDWR);

  if (fd < 0) {
    perror("Failed to open device\n");
    return errno;
  }
  return fd;
}


int test_id(void)
{
  int ret, fd;
  char *wrong = "hello";

  fd = open_file("/sys/kernel/debug/eudyptula/id");
  printf("Attempting read test\n");
  ret = read(fd, &id, BUFFER_LEN);
  close(fd);
  if (ret < 0) {
    perror("Failed to read from device");
    return errno;
  }
  printf("Received id is %s\n", id);

  fd = open_file("/sys/kernel/debug/eudyptula/id");
  printf("Attempting correct write test\n");
  ret = write(fd, &id, strlen(id));
  close(fd);
  if (ret < 0) {
    perror("Failed to write to device\n");
    return errno;
  }

  printf("SUCCESS: Write succeeded\n");

  printf("Attempting wrong write test\n");
  fd = open_file("/sys/kernel/debug/eudyptula/id");
  ret = write(fd, wrong, strlen(wrong));
  close(fd);
  if (ret < 0) {
    perror("SUCCESS: Failed to write to device\n"); //failure is expected
  }

  return 0;
}

int test_jiffies()
{
  int ret, fd;
  char jiffies[JIFFIES_LEN] = { 0 };

  printf("Attempting jiffies test\n");
  fd = open_file("/sys/kernel/debug/eudyptula/jiffies");
  ret = read(fd, &jiffies, JIFFIES_LEN);
  close(fd);
  if (ret < 0) {
    perror("Failed to read jiffies");
    return errno;
  }
  printf("Current jiffies is: %s\n", jiffies);
  return 0;
}


int test_foo()
{
  int i, ret, fd;
  char send[JIFFIES_LEN] = { 0 };
  char recv[JIFFIES_LEN] = { 0 };

  for(i=0; i < JIFFIES_LEN; i++) {
    send[i] = 0xA5;
  }

  printf("Attempt to open foo file\n");
  fd = open_file("/sys/kernel/debug/eudyptula/foo");
  ret = write(fd, &send, strlen(send));
  close(fd);
  if (ret < 0) {
    perror("Failed to write to device\n");
    return errno;
  }

  fd = open_file("/sys/kernel/debug/eudyptula/foo");
  ret = read(fd, &recv, JIFFIES_LEN);
  close(fd);
  if (ret < 0) {
    perror("Failed to read from device");
    return errno;
  }

  for(i=0; i < JIFFIES_LEN; i++) {
    if(send[i] != recv[i]) {
      printf("INCORRECT! 0x%02x != 0x%02x \n", send[i],recv[i]);
    }
  }

  printf("correctly wrote and read to foo\n");

  return 0;
}



int main(void)
{
  int ret;

  ret = test_id();
  if(ret != 0)
    return ret;

  ret = test_jiffies();
  if(ret != 0)
    return ret;

  ret = test_foo();
  if(ret != 0)
    return ret;

  return 0;
}
