#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/usb.h>
#include <linux/hid.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Frank Vanbever");
MODULE_DESCRIPTION("Hello World module");

static int __init hello_init(void)
{
	pr_info("Hello World! - keyboard probed\n");
	return 0;
}

static void __exit hello_cleanup(void)
{
	pr_info("Goodbye!\n");
}

module_init(hello_init);
module_exit(hello_cleanup);

static struct usb_device_id usb_eudyptula_id_table[] = {
	{ USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID,
	  USB_INTERFACE_SUBCLASS_BOOT, USB_INTERFACE_PROTOCOL_KEYBOARD) },
	{}
};

MODULE_DEVICE_TABLE(usb, usb_eudyptula_id_table);

